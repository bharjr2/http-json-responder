## Introduction

This project allows you to send HTTP GET requests to multiple addresses (using stdin)
and retrieves the HTTP headers in the response.

## Requirements & Environment

This project was written in Python 2.7.9 (though python 2.6.x and 2.7.x should be compatible.)

Required python modules used in this project:

* requests (see [here](http://docs.python-requests.org/en/master/user/install/#install) for installation instructions  )

## Download

Instantly download the project with the git command:

```
git clone https://gitlab.com/bharjr2/http-json-responder.git
```

You may also download this project in a zip or tar archive from:

```
https://gitlab.com/bharjr2/http-json-responder
```

## Example Usage

Navigate to the project root directory and run main.py in python.

```
cd http-json-responder/
python main.py
```

When prompted, enter a valid URL. If you do not wish to add any more URLs, simply press enter without entering any text.

For example:
```
http://www.google.com/
https://www.bing.com
```

Invalid URLs will return a JSON object containing the URL and error.

HTTP GET requests will be sent to valid URLs, and JSON containing information about the headers in the HTTP response will be printed. They can also be viewed as a file in the folder 'output/'.

## Unit testing

While in the project root directory, run:

```
python ut.py
```

If you are not connected to the internet, unit tests which require an internet connection will be skipped.

## Author and Contact Information

Ravi Bharj

ravi_bharj@live.com
