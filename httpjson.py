import requests
import json
import sys

validResponse = []

# Checks if the input URL begins with http:// or https://
# Checks for invalid characters
def checkValidURL(inputURL):
    if inputURL[:7]=='http://' or inputURL[:8]=='https://':
        # Ensure that each character in the string is acceptable 
        for character in inputURL:
            o = ord(character)
            if o<32 or o>126:
                return False
        return True
    return False

# Create JSON from HTTP response headers after a GET request.
# Or, if no response is given, create JSON with the URL & error description.
def urlRequestHeadersJSON(inputURL, customTimeout=10):
    jsonData = {}
    jsonData['url'] = inputURL
    try:
        r = requests.get(inputURL, timeout=customTimeout)
        jsonData['status_code'] = r.status_code
        jsonData['date'] = r.headers['date']
        jsonData['content-length'] = r.headers['content-length']
        validResponse.append(jsonData)
    # on connection timeout
    except requests.exceptions.Timeout:
        jsonData['error'] = 'Timeout'
    # on connection failure
    except requests.exceptions.ConnectionError:
        jsonData['error'] = 'Connection Error'
    # other failures
    except:
        jsonData['error'] = 'other'
        sys.stderr.write('Unhandled exception, unknown error when while connecting.\n')
    return json.dumps(jsonData, sort_keys=True, indent=4, separators=(',', ': '))

# function used to quickly create a JSON response
def jsonBadResponse(inputURL, error):
    jsonData = {}
    jsonData['url'] = inputURL
    jsonData['error'] = error
    return json.dumps(jsonData, sort_keys=True, indent=4, separators=(',', ': '))

# Sort by status code
def sortValid():
    # First create a list of status codes used (vals) O(n)
    vals = []
    for response in validResponse:
        code = response["status_code"]
        if code not in vals: vals.append(code)

    # put the list of status codes in order
    vals = sorted(vals)

    # For each code used, append the responses which match that code. O(n^2)
    newResponse = []
    for code in vals:
        for response in validResponse:
            if code==response["status_code"]:
                newResponse.append(response)

    jsonString = json.dumps(newResponse, sort_keys=True, indent=4, separators=(',', ': '))
    return jsonString
