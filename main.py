import httpjson as hj
import os
import shutil

# Create directory and start filename counter
counter = 0
outputPath = "output/"
if os.path.exists(outputPath):
    shutil.rmtree(outputPath)
os.makedirs(outputPath)

print "Write as many URLs as you would like. Press enter (without entering any text) to send the GET requests."

# Keep asking for an address
validURL, invalidURL = [], []
while True:
    inputURL = raw_input("Please enter a URL: ")
    # If nothing was entered, break out of the loop
    if len(inputURL)==0:
        break
    if hj.checkValidURL(inputURL)==True:
        validURL.append(inputURL)
    else:
        invalidURL.append(inputURL)

# For each invalid URL, create the JSON response, print, and write to file
for url in invalidURL:
    rsp = hj.jsonBadResponse(url, 'Invalid URL')
    print rsp
    fp = open(outputPath+str(counter), 'w')
    fp.write(rsp)
    fp.close()
    counter += 1

# For each valid URL, send a GET request
# put the headers into a JSON response (or create one if there is an error), print, and write to file
for url in validURL:
    rsp = hj.urlRequestHeadersJSON(url)
    print rsp
    fp = open(outputPath+str(counter), 'w')
    fp.write(rsp)
    fp.close()
    counter += 1

# Sort valid URLs, print, and write to file.
print "Sorted responses:"
rsp = hj.sortValid()
print rsp
fp = open(outputPath+'sorted', 'w')
fp.write(rsp)
fp.close()

exit(0)
