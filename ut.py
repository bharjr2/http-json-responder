#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import json
import httpjson as hj

class offlineTests(unittest.TestCase):
    def test_checkValidURL_http(self):
        result = hj.checkValidURL('http://www.google.com')
        self.assertTrue(result)

    def test_checkValidURL_https(self):
        result = hj.checkValidURL('https://www.google.com')
        self.assertTrue(result)

    def test_checkValidURL_noProtocol(self):
        result = hj.checkValidURL('www.google.com')
        self.assertFalse(result)

    def test_checkValidURL_invalidCharacters(self):
        result = hj.checkValidURL('http://www.españa.com')
        self.assertFalse(result)

    def test_jsonBadResponse_testError(self):
        req = hj.jsonBadResponse('http://www.google.com', 'testError')
        result = json.loads(req)['error']
        self.assertEqual(result, 'testError')

class onlineTests(unittest.TestCase):
    def test_urlRequestHeadersJSON_working(self):
        req = hj.urlRequestHeadersJSON('http://www.google.com')
        result = json.loads(req)['status_code']
        self.assertEqual(result, 200)

    # if the URL passes checks but still can't connect
    def test_urlRequestHeadersJSON_badAddress(self):
        req = hj.urlRequestHeadersJSON('http://www.non-exsistent_website.org.av')
        result = json.loads(req)['error']
        self.assertEqual(result, 'Connection Error')

    # Because there is no website which reliably times out, the time to wait is reduced to 0.001s
    # This simulates what would occur should a website not respond to a GET request within 10s.
    def test_urlRequestHeadersJSON_timeout(self):
        req = hj.urlRequestHeadersJSON('http://www.google.com', 0.001)
        result = json.loads(req)['error']
        self.assertEqual(result, 'Timeout')

                        
if __name__=='__main__':
    import requests
    import sys
    try:
        r = requests.get('http://www.google.com')
        nextTests = unittest.TestLoader().loadTestsFromTestCase(onlineTests)
        unittest.TextTestRunner(verbosity=2).run(nextTests)
    except:
        sys.stderr.write('Could not connect to the internet. Skipping online tests..\n')

    nextTests = unittest.TestLoader().loadTestsFromTestCase(offlineTests)
    unittest.TextTestRunner(verbosity=2).run(nextTests)
